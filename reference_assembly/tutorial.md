# Assembly by reference 

In contrast to *de novo* methods, reference-based assembly methods use, as the name suggests, a reference sequence to align the reads and generate a consensus sequence. Again, there are many assemblers that
are publicly available, though the most popular are **bwa-mem** and **LAST**. This tutorial focuses on using the **bwa-mem** tool along with **samtools** and **bcftools** to identify sets of variants in the
aligned reads.

## 1. Run BWA-MEM
**bwa-mem**

## 2. Run SAMtools to operate on SAM/BAM files
SAMtools is a collection of programs that can:

* Convert alignments from ```.sam``` (text) to ```.bam``` (binary) formats
* Index reference sequence and index/sort alignments
* Pileup reads for variant calling

All programs within SAMtools are accessed under the following syntax:
~~~
samtools [tool] [...]
~~~

#### 2a. Index the reference FASTA file
To start, we index the reference sequence's FASTA file by using the ```faidx``` command; this generates a file with the ```.fai``` extension. Let's use the [pph1448a genome](../dat/pph1448a_complete.fna) as the reference FASTA file

~~~
# samtools faidx [REFERENCE.fasta]
samtools faidx ../dat/pph1448a_complete.fna
~~~

This should generate a new file named ```pph1448a_complete.fna.fai```. This is a tab-delimited file that can be used by SAMtools later.

#### 2b. Convert the SAM file into a BAM file
Once we have the indexed FASTA file, we then run the ```view``` command to convert our sam file into a bam file format. The bam file format is binary, and it is used by other programs within samtools to
record variants between our aligned reads with respect to the reference sequence. Going with our example using the pph1644r sam file,

~~~
# samtools view -b -t [REFERENCE.fasta.fai] -o [OUTPUT.bam] [INPUT.sam]
samtools view -b -t pph1448a_complete.fna.fai -o pph1644r_aligned.bam pph1644r_aligned.sam
~~~

This generates a new file named ```pph1644r_aligned.bam```. The ```-b``` flag tells ```samtools view``` to generate a binary (bam) format file. The ```-t``` flag is used to provide a tab-delimited file
(in this case, ```pph1448a_complete.fna.fai```). The ```-o``` flag indicates the output bam file name.


