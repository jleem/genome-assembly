#!/usr/bin/env python

"""
rip_sequences.py
Description: 	
Mar 22, 2017
"""
from argparse import ArgumentParser

argParser = ArgumentParser()
argParser.add_argument("-f", help = "Input FASTA file for ripping individual sequences.")
argParser.add_argument("-n", help = "Provide a name for your job.", default = "output")
argParser.add_argument("--id", "-i", dest = "i", help = "Provide an ID for each of your sequences. By default, we'll use the name of the original file.", default = None)
options = argParser.parse_args()

from Bio import SeqIO
import os
if not os.path.isdir(options.n):
    os.mkdir(options.n)

def rip_record(r):
    STRING = ">%s\n%s" % (r.description, r.seq)
    return STRING

if not options.i:
    genome_id = options.f.split("/")[-1]
    options.i = os.path.splitext(genome_id)[0]

#
seqfile = SeqIO.parse(open(options.f), 'fasta')

for i, record in enumerate(seqfile):
    s = rip_record(record)

    with open("%s/%s_%d.fasta" % (options.n, options.i, i), 'w') as outfile:
        print >> outfile, s
