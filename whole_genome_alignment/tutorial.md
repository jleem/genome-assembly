# Whole genome alignment (WGA) tutorial

Aligning whole genomes provides us an idea of the synteny of the compared genomes (*i.e.* their relative gene orientations) and if there are any large-scale changes between the compared genomes.
Furthermore, especially for *de novo* assembly methods, WGA provides us a method for ordering contigs. This tutorial will focus on **prokaryotic genomes**.

There are multiple methods for WGA, though this tutorial will mostly focus on using **MUMmer** and **abacas** to generate a consensus sequence.

## Aligning a draft genome and reference ##
Draft genomes, such as that of *P. syringae pv. phaseolicola* 1644R, are traditionally in [scaffold form](pph1644r_draft.fna). Complete genomes, such as that of *P. syringae pv. phaseolicola* 1448A,
have the complete genome as a linear sequence of nucleotides for the chromosome and plsamid(s). Under the assumption that your draft genome of interest is evolutionarily close to your reference, WGA
is a great method of linearising your sequence.

**MUMmer** is a package of tools, and it's capable of aligning genomes, and producing plots that show the alignment between contig(s) and the reference sequence of interest. **abacas** uses MUMmer in
order to assemble the contigs into a linearised consensus sequence.

### 1. Run MUMmer ###
Let's first run **nucmer**, which is part of the **MUMmer** package, to check that aligning our draft and reference is suitable. The aligned genome will then be used to plot the alignment between the
draft and the reference.
~~~
# nucmer --prefix=[JOB NAME] [REFERENCE] [DRAFT]

nucmer --prefix=psp_phaseolicola pph1448a_complete.fna pph1644r_draft.fna

mummerplot --png psp_phaseolicola.delta -R pph1448a_complete.fna -Q pph1644r_draft.fna --filter --layout
~~~

The plot seems to have a decent diagonal, suggesting that the individual contigs map fairly well along the entire genome.

### 2. Run ABACAS ###
**abacas** is a script provided by the Sanger institute, which calls **nucmer** to order the contigs. However, **abacas** can only deal with one reference sequence at a time; *e.g.* if your reference
genome contains a chromosome and two plasmids, we have to split them out. Use the [rip_sequences.py](../rip_sequences.py) script to rip out the individual sequences in your reference FASTA file.

~~~
python ../rip_sequences.py -f pph1448_complete.fna -n output
~~~
