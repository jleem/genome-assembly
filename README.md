# Genome Assembly Tutorial #

This tutorial is aimed at assembling bacterial genomes, and analysing the gene repertoire.
The assumption is that genomic data is from Nanopore sequencing; some of these tools may/may not work with Illumina.
(c) Created by Jinwoo Leem and Gail Preston, 2017.

## Software requirements ##

Genome assembly and analysis depends on a wide range of software and scripts; we use the tools listed below.
We have also listed some alternatives.

Provided that all the tools are installed on your machine, all the steps (apart from *de novo* assembly) should run within 2 hours on a standard laptop with 8GB RAM.

**Basic dependencies**

* Canu v. 1.3 or higher
* LAST 843 or higher; BWA
* MUMmer v. 3.2 or higher
* ABACAS v. 1.31 or higher
* SAMtools and BCFtools v. 1.4 or higher
* GeneMark
* BLAST v. 2.6 or higher
* NRPSPredictor2 
* Matplotlib v. 2.0 or higher recommended
* BioPython v. 1.67 or higher
* Pandas v. 0.16 or higher
* GNUplot v. 5.0 or higher

### Genome assembly *de novo* ###
* [Canu](https://github.com/marbl/canu)

#### Other *de novo* assemblers ####
* [Velveth/Velvetg](https://github.com/dzerbino/velvet/wiki/Manual)
* [GraphMap](https://github.com/isovic/graphmap)

### Genome assembly by reference ###
* [LAST](http://last.cbrc.jp/)
* [BWA-MEM](https://github.com/lh3/bwa)

### Whole genome alignment tools to generate consensus sequences ###
* [MUMmer](http://mummer.sourceforge.net/) and [ABACAS](http://abacas.sourceforge.net/)
* [Mauve](http://darlinglab.org/mauve/mauve.html)

### Calling variants ###
* [SAMTools](http://www.htslib.org/download/)
* [BCFTools](http://www.htslib.org/download/)

### Annotating genomes ###
* [GeneMark](http://exon.gatech.edu/GeneMark/gmhmmp.cgi)

### Analysing genomes ###
* [BLAST](ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/)
* [BioPython](http://biopython.org/)
* [NRPSPredictor2](http://nrps.informatik.uni-tuebingen.de/static/download.html)

### Illustrations ###
* [Matplotlib](http://matplotlib.org/)
* [BioPython](http://biopython.org/)
* [Pandas](http://pandas.pydata.org/)
