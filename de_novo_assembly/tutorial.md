# *De novo* Genome Assembly Tutorial #

The 'selling point' of *de novo* assembly is the fact that we do not need a reference sequence, and we can build a genome purely out of the reads from our sequencing device (*e.g.* the Nanopore). There are
several *de novo* assemblers that are available, though this tutorial will focus on using the **canu** assembler. However, a limitation of using canu is its reliance on parallel computing; however, it could,
in theory, be used on a standard laptop with even 4GB memory for prokaryotic genomes.

## Canu algorithm details ##

## 1. Run Canu ##
Canu is pretty straight-forward to run; check the [official documentation][http://canu.readthedocs.io/en/latest/quick-start.html] for further details. 

~~~
./canu -p [JOB NAME] -d [OUTPUT DIRECTORY] genomeSize=[APPROXIMATE GENOME SIZE] -nanopore-raw [READS FASTA FILES]
~~~

## 2. Clean-up Canu ##
Canu self-corrects as it assembles the reads, but a post-cleaning step with tools such as Nanopolish or Pilon can be done.

## 3. Evaluate assembly quality ##
QUAST 
