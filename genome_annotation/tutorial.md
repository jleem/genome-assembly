# Genome Annotation #

Annotating and analysing the contents of our assembled genome is vital to understand the genetic basis of an organism's phenotype. Annotation is typically done by methods using Hidden Markov Models (HMMs);
they are explained in further detail [here](http://www.nature.com/nbt/journal/v22/n10/full/nbt1004-1315.html). Essentially, HMMs place a probability that a stretch of DNA nucleotides is a gene, stop codon,
etc. Once putative genes are identified, they are often BLASTed to a database of gene/protein sequences.

## 1. Run GeneMark
[GeneMark](http://exon.gatech.edu/GeneMark/heuristic_gmhmmp.cgi) is a program that detects genes by using HMMs. There is an online version as well with pre-built heuristic models, but for this tutorial, we
will build the HMMs with our own genome. The script ```run_genemark.sh``` calls two programs of the GeneMark suite: ```gmsn.pl``` and ```gmhmmp.pl```. Once the HMMs are built by ```gmsn.pl```, ```gmhmmp.pl```
annotates your input genome and returns a series of putative genes, with either the DNA sequence (output file ends in ```.fnn```) or the translated sequence (file ends in ```.faa```).


~~~
./run_genemark.sh [NAME OF GENEMARK HMM] [ASSEMBLED GENOME FASTA] [REFERENCE FOR BUILDING GENEMARK]
~~~
